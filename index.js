fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(todos => console.log(todos))

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(todos => {

  	let titles = todos.map(todo => todo.title)
  	console.log(titles)
 })

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then(response => response.json())
.then(onetodo => console.log(onetodo))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(onetodo => console.log(`The item ${onetodo.title} on the list has a status of ${onetodo.completed ? 'Completed': 'Not Completed'}`))

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'This is a title',
		body: 'This is a body',
		userId: 1
	})
})
.then(response => response.json())
.then(updatedtodo => console.log(updatedtodo))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: 'Pending',
		description: 'This is a description',
		status: 'Pending',
		title: 'Update to do list',
		userId: 1
	})
})
.then(response => response.json())
.then(updatedtodo => console.log(updatedtodo))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: '01/01/2023',
		status: 'Completed',
		title: 'This is a title',
		userId: 1
	})
})
.then(response => response.json())
.then(hello => console.log(hello))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(response => response.json())
.then(hello => console.log(hello))
